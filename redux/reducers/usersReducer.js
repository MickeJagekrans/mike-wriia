export default function usersReducer(users = [], action) {
  switch(action.type) {
    case 'GET_USERS':
      return action.users;
    default:
      return users
  }
};
