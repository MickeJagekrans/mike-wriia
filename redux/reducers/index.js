import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import roundReducer from './roundReducer';
import usersReducer from './usersReducer';

const rootReducer = combineReducers({
  routing: routerReducer,
  round: roundReducer,
  users: usersReducer
});

export default rootReducer;
