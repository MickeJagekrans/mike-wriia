export default function roundReducer(round = {}, action) {
  switch(action.type) {
    case 'GET_NEXT_ROUND':
      return Object.assign({}, round, {
        user: action.user
      });
    case 'ADD_USER_TO_ROUND':
      return Object.assign({}, round, {
        usersInRound: [action.userId, ...round.usersInRound]
      });
    case 'REMOVE_USER_FROM_ROUND':
      return Object.assign({}, round, {
        usersInRound: round.usersInRound.filter((userId) => {
          return action.userId != userId;
        })
      });
    case 'CLEAR_ROUND':
      return Object.assign({}, round, {
        usersInRound: []
      });
    default:
      return round;
  }
}
