import helpers from './helpers';

const userActions = {

  getUsers: function () {
    return helpers.getData('/users', 'GET_USERS', 'users');
  }

};

export default userActions;
