import helpers from './helpers';

let roundActions = {

  getNextRound: function (usersInRound) {
    return helpers.postData('/nextRound', 'GET_NEXT_ROUND', 'user', {
      method: 'post',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ usersInRound })
    });
  },

  addUserToRound: function (userId) {
    return {
      type: 'ADD_USER_TO_ROUND',
      userId: userId
    };
  },

  removeUserFromRound: function (userId) {
    return {
      type: 'REMOVE_USER_FROM_ROUND',
      userId: userId
    };
  },

  clearRound: function () {
    return { type: 'CLEAR_ROUND' };
  }

};

export default roundActions;
