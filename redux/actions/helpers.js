let baseUrl = 'http://localhost:3001';

function json(res) {
  return res.json();
}

function getData(path, type, prop) {
  return (dispatch) => {
    return new Promise((resolve, reject) => {
      fetch(baseUrl + path).then(json, reject).then((data) => {
        let result = { type };
        result[prop] = data[prop];
        dispatch(result);
        resolve();
      }, reject);
    });
  };
}

function postData(path, type, prop, options) {
  return (dispatch) => {
    return new Promise((resolve, reject) => {
      fetch(baseUrl + path, options).then(json, reject).then((data) => {
        let result = { type };
        result[prop] = data[prop];
        dispatch(result);
        resolve();
      });
    });
  };
}

const helpers = {
  getData: getData,
  postData: postData
};

export default helpers;
