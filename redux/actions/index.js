import roundActions from './roundActions';
import userActions from './userActions';

export default Object.assign({}, roundActions, userActions);
