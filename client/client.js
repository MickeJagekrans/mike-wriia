import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route, IndexRedirect, hashHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';

import configureStore from '../redux/store';
import actions from '../redux/actions';
import App from '../components/App';
import Login from '../components/Login';
import NewRound from '../components/NewRound';
import Result from '../components/Result';

let initialState = {
  round: {
    user: {},
    usersInRound: [],
    usersNotInRound: []
  }
};

const store = configureStore(initialState);
const history = syncHistoryWithStore(hashHistory, store);

render(
  <Provider store={store}>
    <Router history={history}>
      <Route path="/" component={App}>
        <IndexRedirect to="/login" />
        <Route path="login" component={Login}/>
        <Route path="newRound" component={NewRound}/>
        <Route path="result" component={Result}/>
      </Route>
    </Router>
  </Provider>,
  document.getElementById('app')
);
