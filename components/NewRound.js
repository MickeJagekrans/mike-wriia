import React, { Component } from 'react';
import { hashHistory } from 'react-router';
import componentBuilder from './componentBuilder';
import RoundUser from './RoundUser';

class NewRound extends Component {

  constructor(props, context) {
    super(props, context);
    this.props.actions.getUsers();
  }

  calculate() {
    this.props.actions.getNextRound(this.props.round.usersInRound).then(() => {
      hashHistory.push('/result');
    });
  }

  render() {
    let usersInRound = this.props.users.filter((user) => {
      return this.props.round.usersInRound.indexOf(user.id) > -1;
    });

    let usersNotInRound = this.props.users.filter((user) => {
      return this.props.round.usersInRound.indexOf(user.id) == -1;
    });

    return (
      <div>
        <div>Round builder page</div>
        {
          usersInRound.map((user, idx) => {
            return <RoundUser key={idx} user={user} inRound={true} actions={this.props.actions}/>;
          })
        }
        {
          usersNotInRound.map((user, idx) => {
            return <RoundUser key={idx} user={user} inRound={false} actions={this.props.actions}/>;
          })
        }
        <button onClick={this.calculate.bind(this)}>Calculate</button>
      </div>
    );
  }

}

export default componentBuilder(NewRound);
