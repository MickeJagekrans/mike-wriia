import React, { Component } from 'react';
import { hashHistory } from 'react-router';

class Login extends Component {

  constructor(props, context) {
    super(props, context);
  }

  login() {
    hashHistory.push('/newRound');
  }

  render() {
    return (
      <div>
        <input type="text" placeholder="username"/><br/>
        <input type="password" placeholder="password"/><br/>
        <button onClick={this.login.bind(this)}>Login</button>
      </div>
    );
  }

}

export default Login;
