import React, { Component } from 'react';

class RoundUser extends Component {

  constructor(props, context) {
    super(props, context);
  }

  addToRound(userId) {
    this.props.actions.addUserToRound(userId);
  }

  removeFromRound(userId) {
    this.props.actions.removeUserFromRound(userId)
  }

  render() {
    let { user, inRound } = this.props;
    let method = inRound ? this.removeFromRound.bind(this) : this.addToRound.bind(this);

    const style = {
      backgroundColor: inRound ? 'lightgreen' : 'none',
      margin: '5px',
      padding: '5px',
      cursor: 'pointer'
    };

    return (<div style={style}
                onClick={() => method(user.id)}>
             {user.name} ({user.beers})
           </div>);
  }

}

export default RoundUser;
