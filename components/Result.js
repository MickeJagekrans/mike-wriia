import React, { Component } from 'react';
import { hashHistory } from 'react-router';
import componentBuilder from './componentBuilder';

class Result extends Component {

  constructor(props, context) {
    super(props, context);
  }

  nextRound() {
    this.props.actions.clearRound();
    hashHistory.push('/newRound');
  }

  render() {
    return (
      <div>
        <div>It is {this.props.round.user.name}'s round!</div>
        <button onClick={this.nextRound.bind(this)}>Next round</button>
      </div>
    );
  }

}

function mapStateToProps(state) {
  return {
    round: state.round
  };
}

export default componentBuilder(Result, mapStateToProps);
