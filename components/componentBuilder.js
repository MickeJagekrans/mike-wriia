import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import actions from '../redux/actions';

function defaultMapStateToProps(state) {
  return state;
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default function(Component, mapStateToProps = defaultMapStateToProps) {
  return connect(mapStateToProps, mapDispatchToProps)(Component);
}
