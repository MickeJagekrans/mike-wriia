'use strict';

let users = [{
  name: 'Fred',
  beers: 0,
  id: 0
}, {
  name: 'Dan',
  beers: 0,
  id: 1
}, {
  name: 'Jonny',
  beers: 0,
  id: 2
}];

function getUsers() {
  return users;
}

function calculateRound(userIds) {
  return users[2];
}

module.exports = {
  getUsers: getUsers,
  calculateRound: calculateRound
};
