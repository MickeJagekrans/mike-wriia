'use strict';

let restify = require('restify');
let app = restify.createServer();
let db = require('./db');

app.use(restify.CORS());
app.use(restify.bodyParser());


app.post('/nextRound', (req, res, next) => {
  let user = db.calculateRound(req.body);
  res.json({ user });
  next();
});

app.get('/users', (req, res, next) => {
  let users = db.getUsers();
  res.json({ users });
  next();
});

app.listen(3001, () => {
  console.log('app server listening on port 3001');
});
