var webpack = require('webpack');

module.exports = {
  devtool: 'cheap-module-source-map',
  entry: ['./client/client.js'],
  plugins: [
    new webpack.optimize.UglifyJsPlugin({ minimize: true }),
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('production')
      }
    })
  ],
  presets: ['react', 'es2015']
};
