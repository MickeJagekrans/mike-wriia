var configVars = require('./config/' + (process.env.NODE_ENV || 'dev') + '.config.js');

module.exports = {
  devtool: configVars.devtool,
  entry: configVars.entry,
  output: {
    path: require('path').resolve('./dist'),
    filename: 'bundle.js',
    publicPath: '/'
  },
  plugins: configVars.plugins,
  module: {
    loaders: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query: {
          presets: configVars.presets
        }
      }
    ]
  }
};
